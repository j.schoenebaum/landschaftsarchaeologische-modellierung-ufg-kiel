# Landschaftsarchäologische Modellierung - UFG-Kiel

## Überblick

Dieses Repository dient der Veröffentlichung meiner Prüfungsleistung für die Übung und Vorlesung "Landschaftsarchäologische Modellierung" von Oliver Nakoinz am Institut für Ur- und Frühgeschichte der CAU-Kiel. Das Portfolio besteht aus Methoden zu fünf Kategorien von Landschaftsmodelling, die kurz erklärt und dann in R-Code umgesetzt wurden. Den Methoden liegt die Veröffentlichung von Oliver Nakoinz und Daniel Knitter "Modelling Human Behaviour in Landscapes. Basic Concepts and Modelling Elements (New York 2016)" zugrunde, in der noch detaillierter auf die einzelnen Techniken eingegangen wird. Dieses Portfolio bildet meinen Einstieg in archäologische Modellierung und ist hauptsächlich als ein Experimentieren mit Code und Daten zu verstehen. Von einer Interpretierung der Ergebnisse sollte daher abgesehen werden.

## Ordnerstruktur

### Data 

- verwendeten Datensätze in CSV-Format

### R-Code

- Code nach Kapiteln und Methoden sortiert

### Skript

- fertiges Skript als .qmd und HTML-Output





29.09.2023, Kiel - Kai Julian Schönebaum