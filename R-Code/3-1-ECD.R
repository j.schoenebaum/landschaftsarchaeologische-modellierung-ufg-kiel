# Dichte ECD

hoe_tv <- hoe_sf[, -which(names(hoe_sf) == "Gemeinde")]
hoe_tv  <- terra::vect(hoe_sf)
hoe_vor <- terra::voronoi(
  x        = hoe_tv,
  as.lines = F
)
terra::plot(hoe_vor)

voronodes    <- terra::crds(hoe_vor, 
                            df = T)
voronodes    <- voronodes[,c("x","y")][!duplicated(voronodes[,c("x","y")]), ]
voronodes    <- voronodes[voronodes$x > ag[1]  &  voronodes$x < ag[2], ]
voronodes    <- voronodes[voronodes$y > ag[3]  &  voronodes$y < ag[4], ]
voronodes_sf <- sf::st_as_sf(
  hoe_df, 
  coords = c(3, 2), 
  crs    = "+proj=utm +zone=33 +datum=WGS84 +units=m +no_defs"
)
voronodes <- terra::vect(voronodes_sf)

nnLines      <- terra::nearest(x     = voronodes,  
                               y     = terra::vect(hoe_sf),
                               lines = T)
nnLength     <- terra::perim(nnLines)
voronodes_sf <- cbind(voronodes_sf, 
                      relDens = 1000/nnLength)

dens_ecd <- dem
tps      <- fields::Tps(as.matrix(sf::st_coordinates(voronodes_sf)), voronodes_sf$relDens)

dens_ecd <- terra::interpolate(
  object = terra::rast(dens_ecd), 
  model  = tps, 
  fun    = predict) # Quelle der Fehler?

terra::plot(dens_ecd, 
            col = gray.colors(25, 
                              start = 0.97, 
                              end   = 0.4))
terra::contour((dens_ecd),
               nlevels = 10,
               add      = T)
points(hoe_df$x, 
       hoe_df$y, 
       pch = 16, 
       cex = 0.4)  